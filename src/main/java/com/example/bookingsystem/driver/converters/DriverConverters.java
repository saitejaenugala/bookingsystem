package com.example.bookingsystem.driver.converters;

import com.example.bookingsystem.driver.model.dto.DriverDto;
import com.example.bookingsystem.driver.model.entity.Driver;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DriverConverters {
    public DriverDto driverToDto(Driver driver) {
        DriverDto dto = new DriverDto();
        BeanUtils.copyProperties(driver, dto);
        return dto;
    }
    public List<DriverDto> driverToDto(List<Driver> drivers) {
        return drivers.stream().map(driver -> driverToDto(driver)).collect(Collectors.toList());
    }

    public Driver dtoToDriver(DriverDto dto) {
        Driver driver = new Driver();
        BeanUtils.copyProperties(dto, driver);
        return driver;
    }
}
