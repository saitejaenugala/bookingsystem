package com.example.bookingsystem.driver.controller;

import com.example.bookingsystem.driver.converters.DriverConverters;
import com.example.bookingsystem.driver.model.dto.DriverDto;
import com.example.bookingsystem.driver.model.entity.Driver;
import com.example.bookingsystem.driver.service.DriverService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/driver")
@AllArgsConstructor
public class DriverController {

    private DriverService driverService;
    private DriverConverters driverConverter;

    @PostMapping("/create")
    public ResponseEntity<DriverDto> createDriver(@RequestBody DriverDto driverDto) {
        Driver createdDriver = driverService.createDriver(driverConverter.dtoToDriver(driverDto));
        DriverDto dto = driverConverter.driverToDto(createdDriver);
        return ResponseEntity.ok(dto);
    }

    @GetMapping
    public List<DriverDto> getAll() {
        List<DriverDto> driverDtos = driverConverter.driverToDto(driverService.getAllDrivers());
        return driverDtos;
    }
}
