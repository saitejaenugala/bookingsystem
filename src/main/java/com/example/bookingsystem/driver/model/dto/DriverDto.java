package com.example.bookingsystem.driver.model.dto;

import lombok.Data;

@Data
public class DriverDto {
    private Long id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Long phoneNumber;
}
