package com.example.bookingsystem.cities.model.dto;

import lombok.Data;

@Data
public class CitiesDto {
    private Long id;
    private String name;
    private String code;
    private  String state;
}
