package com.example.bookingsystem.cities.converter;

import com.example.bookingsystem.cities.model.dto.CitiesDto;
import com.example.bookingsystem.cities.model.entity.Cities;
import org.springframework.beans.BeanUtils;

public class CitiesConverter {
    public CitiesDto entityToDTO(Cities cities) {
        CitiesDto citiesDto = new CitiesDto();
        BeanUtils.copyProperties(citiesDto, citiesDto);
        return citiesDto;
    }

    public Cities dtoToEntity(CitiesDto citiesDto) {
        Cities cities = new Cities();
        BeanUtils.copyProperties(citiesDto, cities);
        return cities;
    }
}

