package com.example.bookingsystem.busType.controller;

;
import com.example.bookingsystem.busType.converter.BusTypeConverter;
import com.example.bookingsystem.busType.model.dto.BusTypeDto;
import com.example.bookingsystem.busType.model.entity.BusType;
import com.example.bookingsystem.busType.service.BusTypeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/busType")
public class BusTypeController {

    private BusTypeService busTypeService;
    private BusTypeConverter busTypeConverter;

    @PostMapping("/create")
    public ResponseEntity<BusTypeDto> createBus(@RequestBody BusTypeDto dto) {
        BusType busType = busTypeService.createBusType(busTypeConverter.dtoToBusType(dto));
        BusTypeDto busTypeDto = busTypeConverter.busTypeToDto(busType);
        return ResponseEntity.ok(busTypeDto);
    }

    @GetMapping("/get")
    public List<BusTypeDto> getAll() {
        return busTypeConverter.busTypeToDto(busTypeService.getAllBusTypes());
    }

}

