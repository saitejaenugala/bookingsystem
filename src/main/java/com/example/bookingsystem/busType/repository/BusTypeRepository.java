package com.example.bookingsystem.busType.repository;

import com.example.bookingsystem.busType.converter.BusTypeConverter;
import com.example.bookingsystem.busType.model.entity.BusType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusTypeRepository extends JpaRepository<BusType,Long> {
}
