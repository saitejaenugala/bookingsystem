package com.example.bookingsystem.busType.model.dto;

import lombok.Data;

@Data
public class BusTypeDto {
    private Long id;
    private String name;
    private Integer seatCount;
}
