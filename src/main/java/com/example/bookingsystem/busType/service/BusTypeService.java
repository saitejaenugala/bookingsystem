package com.example.bookingsystem.busType.service;

        import com.example.bookingsystem.busType.model.entity.BusType;
        import com.example.bookingsystem.busType.repository.BusTypeRepository;
        import lombok.AllArgsConstructor;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        import java.util.List;
@Service
@AllArgsConstructor
public class BusTypeService {
    private BusTypeRepository busTypeRepository;
    public BusType createBusType(BusType busType) {
        return busTypeRepository.save(busType);
    }

    public List<BusType> getAllBusTypes() {
        return busTypeRepository.findAll();
    }
}
