package com.example.bookingsystem.users.converters;

import com.example.bookingsystem.users.model.dto.UsersDto;
import com.example.bookingsystem.users.model.entity.Users;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class UsersConverters {

    public Users dtoToUser(UsersDto dto) {
        Users users = new Users();
        BeanUtils.copyProperties(dto, users);
        return users;
    }

    public UsersDto userToDto(Users users) {
        UsersDto usersDto = new UsersDto();
        BeanUtils.copyProperties(users, usersDto);
        return usersDto;
    }
}
