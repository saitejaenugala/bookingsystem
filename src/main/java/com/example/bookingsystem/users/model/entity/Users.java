package com.example.bookingsystem.users.model.entity;

import com.example.bookingsystem.bookings.model.entity.Bookings;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table
@Data
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String password;
    private String email;
    private Long mobileNumber;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Bookings> userBookings;

}
