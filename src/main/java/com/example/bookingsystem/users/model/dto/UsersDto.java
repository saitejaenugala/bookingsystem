package com.example.bookingsystem.users.model.dto;

import com.example.bookingsystem.bookings.model.entity.Bookings;
import lombok.Data;

import java.util.List;

@Data
public class UsersDto {
    private Long id;
    private String userName;
    private String password;
    private String email;
    private Long mobileNumber;

    private List<Bookings> userBookings;
}
