package com.example.bookingsystem.bookings.model.dto;


import com.example.bookingsystem.bus.model.dto.BusDto;
import com.example.bookingsystem.enums.Status;
import com.example.bookingsystem.users.model.dto.UsersDto;

import lombok.Data;

import java.util.Date;

@Data
public class BookingsDto {
    private Long id;
    private String fromCity;
    private String toCity;
    private Date travelDate;
    private Date bookingDate;
    private Long seatNo;

    private Status status;
    private BusDto busDetails;
    private UsersDto userDetails;
}
