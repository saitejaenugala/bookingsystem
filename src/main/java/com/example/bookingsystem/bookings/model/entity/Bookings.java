package com.example.bookingsystem.bookings.model.entity;

import com.example.bookingsystem.bus.model.entity.Bus;
import com.example.bookingsystem.enums.Status;
import com.example.bookingsystem.users.model.entity.Users;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Entity
@Table
@Data
public class Bookings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fromCity;
    private String toCity;
    private Date travelDate;
    private Date bookingDate;
    private Long seatNo;

    @Enumerated(EnumType.STRING)
    private Status status;
    @ManyToOne(cascade = CascadeType.ALL)
    private Bus busDetails;
    @ManyToOne(cascade = CascadeType.ALL)
    private Users userDetails;

}
