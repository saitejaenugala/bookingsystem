package com.example.bookingsystem.bookings.converters;

import com.example.bookingsystem.bookings.model.dto.BookingsDto;
import com.example.bookingsystem.bookings.model.entity.Bookings;
import com.example.bookingsystem.bus.converter.BusConverter;
import com.example.bookingsystem.bus.repository.BusRepository;
import com.example.bookingsystem.busType.converter.BusTypeConverter;
import com.example.bookingsystem.users.converters.UsersConverters;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class BookingsConverter {

    private BusConverter busConverter;
    private UsersConverters userConverter;
    private BusRepository busRepository;

    public BookingsDto bookingsToDto(Bookings bookings) {
        BookingsDto dto = new BookingsDto();
        BeanUtils.copyProperties(bookings, dto);
        dto.setBusDetails(busConverter.busToDto(bookings.getBusDetails()));
        dto.setUserDetails(userConverter.userToDto(bookings.getUserDetails()));
        return dto;
    }

    public List<BookingsDto> bookingsToDto(List<Bookings> bookings) {
        return bookings.stream().map(bookings1 -> bookingsToDto(bookings1)).collect(Collectors.toList());
    }

    public Bookings dtoToBookings(BookingsDto dto) {
        Bookings bookings = new Bookings();
        BeanUtils.copyProperties(dto, bookings);
        bookings.setBusDetails(busRepository.findById(dto.getBusDetails().getId()).get());
        bookings.setUserDetails(userConverter.dtoToUser(dto.getUserDetails()));
        return bookings;
    }
}
