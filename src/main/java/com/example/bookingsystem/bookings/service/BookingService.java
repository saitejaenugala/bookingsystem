package com.example.bookingsystem.bookings.service;
import com.example.bookingsystem.bookings.model.dto.BookingsDto;
import com.example.bookingsystem.bookings.model.entity.Bookings;
import com.example.bookingsystem.bookings.repository.BookingsRepository;
import com.example.bookingsystem.busType.model.entity.BusType;
import com.example.bookingsystem.busType.repository.BusTypeRepository;
import com.example.bookingsystem.enums.Status;
import com.example.bookingsystem.users.model.entity.Users;
import com.example.bookingsystem.users.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@org.springframework.stereotype.Service
@AllArgsConstructor
public class BookingService {

    private BookingsRepository bookingsRepository;
    private UsersRepository userRepository;
    private BusTypeRepository busTypeRepository;


    public Bookings createBookings(Bookings bookings) {
        bookings.setBookingDate(new Date());
        BusType busType = busTypeRepository.findById(bookings.getBusDetails().getBusType().getId()).orElse(null);
        if (busType != null && bookings.getSeatNo() <= busType.getSeats()) {
            bookings.setStatus(Status.CONFIRMED);
        } else {
            bookings.setStatus(Status.CANCELLED);
        }
        /*if (bookings.getSeatNo() <= busType.getSeats()) {
            bookings.setStatus(Status.CONFIRMED);
        } else {
            bookings.setStatus(Status.CANCELLED);
        }*/
        return bookingsRepository.save(bookings);
    }


    public ResponseEntity<String> deleteBooking(Long id) {
        bookingsRepository.deleteById(id);
        return new ResponseEntity<>("deleted Successfully", HttpStatus.OK);
    }


    public ResponseEntity<Bookings> updateById(Long id, BookingsDto dto) {
        Bookings existingBookings = bookingsRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Bookings not found"));
        bookingsRepository.save(existingBookings);
        return ResponseEntity.ok(existingBookings);
    }


    public ResponseEntity<Bookings> getBookingsById(Long id) {
        Optional<Bookings> optionalBooking = bookingsRepository.findById(id);
        if (optionalBooking.isPresent()) {
            Bookings booking = optionalBooking.get();
            return ResponseEntity.ok(booking);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public ResponseEntity<Users> getUsersById(Long id) {
        Optional<Users> optionalUsers = userRepository.findById(id);
        if (optionalUsers.isPresent()) {
            Users users = optionalUsers.get();
            return ResponseEntity.ok(users);
        }
        return ResponseEntity.notFound().build();
    }


    public List<Bookings> getBookings() {
        return bookingsRepository.findAll();
    }

}
