package com.example.bookingsystem.bookings.controller;

import com.example.bookingsystem.bookings.converters.BookingsConverter;
import com.example.bookingsystem.bookings.model.dto.BookingsDto;
import com.example.bookingsystem.bookings.model.entity.Bookings;
import com.example.bookingsystem.bookings.service.BookingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/bookings")
@AllArgsConstructor
public class BookingsController {

    private BookingsConverter bookingsConverter;
    private BookingService bookingService;

    @GetMapping("/{Id}")
    public BookingsDto getBookingById(@PathVariable("Id") Long id) {
        return bookingsConverter.bookingsToDto(bookingService.getBookingsById(id).getBody());
    }

    @PostMapping("/create")
    public ResponseEntity<BookingsDto> createBookings(@RequestBody BookingsDto request) {
        Bookings bookingsResponse = bookingService.createBookings(bookingsConverter.dtoToBookings(request));
        BookingsDto response = bookingsConverter.bookingsToDto(bookingsResponse);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete/{Id}")
    public ResponseEntity<String> deleteById(@PathVariable("Id") Long id) {
        return bookingService.deleteBooking(id);
    }

    @PutMapping("/update/{Id}")
    public BookingsDto updateBookingsById(@PathVariable("Id") Long id, @RequestBody BookingsDto dto) {
        Bookings bookings = bookingService.updateById(id, dto).getBody();
        return bookingsConverter.bookingsToDto(bookings);
    }

    @GetMapping("/get")
    public List<BookingsDto> getBookings() {
        List<BookingsDto> bookingsDtoList = bookingsConverter.bookingsToDto(bookingService.getBookings());
        return bookingsDtoList;
    }
}