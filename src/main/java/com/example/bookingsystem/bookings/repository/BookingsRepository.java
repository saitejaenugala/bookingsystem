package com.example.bookingsystem.bookings.repository;

import com.example.bookingsystem.bookings.model.entity.Bookings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingsRepository extends JpaRepository<Bookings,Long> {
}
