package com.example.bookingsystem.bus.converter;


import com.example.bookingsystem.bus.model.dto.BusDto;
import com.example.bookingsystem.bus.model.entity.Bus;
import com.example.bookingsystem.busType.converter.BusTypeConverter;
import com.example.bookingsystem.busType.repository.BusTypeRepository;
import com.example.bookingsystem.driver.converters.DriverConverters;
import com.example.bookingsystem.driver.repository.DriverRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
   @AllArgsConstructor
    @Component
    public class BusConverter {

        private BusTypeConverter busTypeConverter;
        private DriverConverters driverConverter;
        private BusTypeRepository busTypeRepository;
        private DriverRepository driverRepository;

        public BusDto busToDto(Bus bus) {
            BusDto busDto = new BusDto();
            BeanUtils.copyProperties(bus, busDto);
            busDto.setBusType(busTypeConverter.busTypeToDto(bus.getBusType()));
            busDto.setDriver(driverConverter.driverToDto(bus.getDriver()));
            return busDto;
        }

        public List<BusDto> busToDto(List<Bus> buses) {
            return buses.stream().map(bus -> busToDto(bus)).collect(Collectors.toList());
        }

        public Bus dtoToBus(BusDto dto) {
            Bus bus = new Bus();
            BeanUtils.copyProperties(dto, bus);
            bus.setBusType(busTypeRepository.findById(dto.getBusType().getId()).get());
            bus.setDriver(driverRepository.findById(dto.getDriver().getId()).get());
            return bus;
        }
    }
