package com.example.bookingsystem.bus.model.dto;


import com.example.bookingsystem.busType.model.dto.BusTypeDto;
import com.example.bookingsystem.driver.model.dto.DriverDto;
import lombok.Data;

import java.util.Date;
@Data
public class BusDto {
    private Long id;
    private String name;
    private String number;
    private String depot;
    private Date scheduleDate;
    private BusTypeDto busType;
    private DriverDto driver;
}
