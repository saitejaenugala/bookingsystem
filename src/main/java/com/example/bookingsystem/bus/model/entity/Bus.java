package com.example.bookingsystem.bus.model.entity;

import com.example.bookingsystem.busType.model.entity.BusType;
import com.example.bookingsystem.driver.model.entity.Driver;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Table
@Data
public class Bus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String number;
    private String depot;
    private Date scheduleDate;
    @ManyToOne
    private BusType busType;
    @ManyToOne(cascade = CascadeType.ALL)
    private Driver driver;
}
