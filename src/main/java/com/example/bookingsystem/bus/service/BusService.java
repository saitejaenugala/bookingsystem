package com.example.bookingsystem.bus.service;


import com.example.bookingsystem.bus.model.dto.BusDto;
import com.example.bookingsystem.bus.model.entity.Bus;
import com.example.bookingsystem.bus.repository.BusRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BusService {
    private BusRepository busRepository;


    public Bus createBus(Bus bus) {
        return busRepository.save(bus);
    }

    public Bus updateBusById(Long id, BusDto dto) {
        Bus bus = busRepository.getById(id);
        return busRepository.save(bus);
    }

    public ResponseEntity<Bus> getBusById(Long id) {
        Optional<Bus> bus = busRepository.findById(id);
        if (bus.isPresent()) {
            Bus bus1 = bus.get();
            return ResponseEntity.ok(bus1);

        }
        return ResponseEntity.ok().build();
    }

    public List<Bus> getAll() {
        return busRepository.findAll();
    }
}

