package com.example.bookingsystem.bus.controller;

import com.example.bookingsystem.bus.converter.BusConverter;
import com.example.bookingsystem.bus.model.dto.BusDto;
import com.example.bookingsystem.bus.model.entity.Bus;
import com.example.bookingsystem.bus.service.BusService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@AllArgsConstructor
@RestController
@RequestMapping("/api/bus")
public class BusController {
    private BusService busService;

    private BusConverter busConverter;

    @GetMapping("/{Id}")
    public BusDto getBusById(@PathVariable("Id") Long id) {
        return busConverter.busToDto(busService.getBusById(id).getBody());
    }

    @PostMapping("/create")
    public ResponseEntity<BusDto> createBus(@RequestBody BusDto dto) {
        Bus bus = busService.createBus(busConverter.dtoToBus(dto));
        BusDto busDto = busConverter.busToDto(bus);
        return ResponseEntity.ok(busDto);
    }

    @PutMapping("/update/{Id}")
    public BusDto updateBusById(@PathVariable("Id") Long id, @RequestBody BusDto dto) {
        Bus bus = busService.updateBusById(id, dto);
        return busConverter.busToDto(bus);
    }

    @GetMapping("/get")
    public List<BusDto> getBusBookings() {
        List<BusDto> busDtos = busConverter.busToDto(busService.getAll());
        return busDtos;
    }
}